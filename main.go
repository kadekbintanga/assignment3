package main

import (
	"fmt"
	"time"
	"io/ioutil"
	"math/rand"
	"strconv"
	"github.com/go-co-op/gocron"
)

func main(){
	s := gocron.NewScheduler(time.UTC)

	s.Every(5).Seconds().Do(ReloadWeather)

	s.StartAsync()
	// starts the scheduler and blocks current execution path
	s.StartBlocking()
}

func WaterLevelDesc(num int) string {
	if (num <= 5){
		return "Aman"
	}
	if (6 <= num && num <=8){
		return "Siaga"
	}
	if (num > 8 ){
		return "Bahaya"
	}else{
		return "I don't know"
	}
}

func WindLevelDesc(num int) string {
	if (num <= 6){
		return "Aman"
	}
	if (7 <= num && num <=15){
		return "Siaga"
	}
	if (num > 15 ){
		return "Bahaya"
	}else{
		return "I don't know"
	}
}

func ReloadWeather() {
	a := rand.NewSource(time.Now().UnixNano())
	b := rand.New(a)
	water := b.Intn(20)
	waterLevel := WaterLevelDesc(water)
	waterStr := strconv.Itoa(water)
	wind := b.Intn(20)
	windLevel := WindLevelDesc(wind)
	windStr := strconv.Itoa(wind)


	file := fmt.Sprintf(`
	<!DOCTYPE html>
		<html>
		<head>
			<link rel="stylesheet" href="css.bootstrap.css">
			<script src="jquery.js"></script>
		</head>
		<body>
			<h1 center>Dummy Forecast</h1>
			Water : <span id="water">%s</span>
			<br>
			Description : <span id="water-description">%s</span>
			<br>
			Wind : <span id="wind">%s</span>
			<br>
			Description : <span id="wind-description">%s</span>
		</body>
		<script>
			
		</script>
	</html>
	`, waterStr, waterLevel, windStr, windLevel)

	_ = ioutil.WriteFile("forecast.html", []byte(file), 0644)

	fmt.Println("===================New Forecast Data Updated===================")
}